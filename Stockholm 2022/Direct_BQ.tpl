﻿___INFO___

{
  "type": "TAG",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "Direct BQ",
  "brand": {
    "id": "brand_dummy",
    "displayName": ""
  },
  "description": "",
  "containerContexts": [
    "SERVER"
  ]
}


___TEMPLATE_PARAMETERS___

[]


___SANDBOXED_JS_FOR_SERVER___

const addEventCallback = require('addEventCallback');
const BigQuery = require('BigQuery');
const getClientName = require('getClientName');
const getEventData = require('getEventData');
const getTimestamp = require('getTimestampMillis');
const JSON = require('JSON');
const log = require('logToConsole');

const ga_session_id = getEventData('ga_session_id') || '';
const ga_session_number = getEventData('ga_session_number') || '';
const ip = getEventData('ip_override') || '';
const client_id = getEventData('client_id') || '';
const user_agent = getEventData('user_agent') || '';
const timestamp = getTimestamp();

addEventCallback((containerId, eventData) => {
  const event = [{
    ts: timestamp,
    sessionid: ga_session_id,
    sessionnr: ga_session_number,
    clientid : client_id,
    user_agent : user_agent   
  }];
  
  BigQuery.insert({
    projectId: "gtm-nxpmpt6-ztjjn",
    datasetId: "mc_stockholm",
    tableId: "serverside_gtm",
  }, event, {}, () => {
    log('BigQuery Success');
  }, (errors) => {
    log('BigQuery Failure');
    log(JSON.stringify(errors));
  });
});

data.gtmOnSuccess();


___SERVER_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "read_event_metadata",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "debug"
          }
        }
      ]
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "read_event_data",
        "versionId": "1"
      },
      "param": [
        {
          "key": "eventDataAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "read_container_data",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "access_bigquery",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedTables",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 3,
                "mapKey": [
                  {
                    "type": 1,
                    "string": "projectId"
                  },
                  {
                    "type": 1,
                    "string": "datasetId"
                  },
                  {
                    "type": 1,
                    "string": "tableId"
                  },
                  {
                    "type": 1,
                    "string": "operation"
                  }
                ],
                "mapValue": [
                  {
                    "type": 1,
                    "string": "*"
                  },
                  {
                    "type": 1,
                    "string": "*"
                  },
                  {
                    "type": 1,
                    "string": "*"
                  },
                  {
                    "type": 1,
                    "string": "write"
                  }
                ]
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios: []


___NOTES___

Created on 24.10.2022, 13:32:25


